package org.core.sprites {
	import org.flixel.FlxG;
	import org.flixel.FlxPoint;
	import org.flixel.FlxSprite;
	
	/**
	 * ...
	 * @author Nico Glas
	 */
	public class NPC extends FlxSprite {
		[Embed(source="../../../../lib/Sprites/spriteSquare.png")]
		private var _img:Class;
		private var _player:Player;
		
		public function NPC(X:Number, Y:Number, player:Player) {
			super(X, Y, _img);
			this._player = player;
			solid = true;
			this.color = 0x00FF00;
		}
		
		override public function update():void {
			super.update();
			velocity.x = velocity.y = 0;
			FlxG.collide(this, _player);
		}
	}
}