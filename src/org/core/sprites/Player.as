package org.core.sprites {
	import flash.geom.ColorTransform;
	import org.flixel.FlxGroup;
	import org.flixel.FlxSprite;
	import org.flixel.FlxG;
	import org.flixel.system.FlxAnim;
	/**
	 * ...
	 * @author Nico Glas
	 */
	public class Player extends FlxSprite {
		
		[Embed(source="../../../../lib/Sprites/spriteSquare.png")]
		private var _imgPlayer:Class;
		public var blendSprite:FlxSprite;
		private var isBlend:Boolean;
		public function Player(X:Number = 0, Y:Number = 0, color:uint = 0xFF0000, blendSprite:FlxSprite = null) {
			super(X, Y,_imgPlayer);
			
			this.color = color;
			this.blendSprite = blendSprite;
			isBlend = (blendSprite == null);
			var runSpeed:uint = 100;
			drag.x = drag.y = runSpeed * 8;
			maxVelocity.x = maxVelocity.y = runSpeed;
			solid = true;
		}
		
		override public function update():void {
			super.update();
			acceleration.x = 0;
			acceleration.y = 0;
			if (FlxG.keys.LEFT){
				facing = LEFT;
				acceleration.x -= drag.x;
			}else if (FlxG.keys.RIGHT) {
				facing = RIGHT;
				acceleration.x += drag.x;
			}
			if (FlxG.keys.DOWN) {
				facing = DOWN;
				acceleration.y += drag.y;
			}else if (FlxG.keys.UP) {
				facing = UP;
				acceleration.y -= drag.y;
			}
			if (!isBlend) {
				
			}
		}
	}
}