package org.core.states {
	import org.flixel.FlxG;
	import org.flixel.FlxU;
	import org.core.sprites.NPC;
	import org.core.sprites.Player;
	import org.flixel.FlxGroup;
	import org.flixel.FlxState;
	
	/**
	 * ...
	 * @author Nico Glas
	 */
	public class PlayState extends FlxState {
		public var player : Player;
		private var blendSprite : Player;
		public var npc:NPC;
		public var playerGroup:FlxGroup;
		public var npcGroup:FlxGroup;
		
		override public function create():void {
			super.create();
			player = new Player(50, 50, 0x0000FF);
			npc = new NPC(100, 50, player);
			
			playerGroup = new FlxGroup();
			npcGroup = new FlxGroup();
			
			playerGroup.add(player);
			playerGroup.add(blendSprite);
			npcGroup.add(npc);
			
			add(playerGroup);
			add(npcGroup);
		}
		override public function update():void {
			super.update();
			
		}
		override public function draw():void {
			super.draw();
		}
	}
}